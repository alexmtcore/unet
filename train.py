import numpy as np
from keras.models import Model
from keras.layers import Input, merge, Convolution2D, MaxPooling2D, UpSampling2D
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from keras.layers.advanced_activations import ELU
from keras.layers import BatchNormalization
import sys
import os
from skimage.io import imread
from keras.models import load_model

def Conv2DReluBatchNorm(n_filter, w_filter, h_filter, inputs):
    return BatchNormalization()(ELU(alpha=0.1)(Convolution2D(n_filter, w_filter, h_filter, border_mode='same')(inputs)))


def get_unet():
	
    K.set_image_dim_ordering('th')
    img_rows = 200
    img_cols = 200

    inputs = Input((1, img_rows, img_cols))
    conv1 = Conv2DReluBatchNorm(32, 3, 3, inputs)
    conv1 = Conv2DReluBatchNorm(32, 3, 3, conv1)
    pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)

    conv2 = Conv2DReluBatchNorm(64, 3, 3, pool1)
    conv2 = Conv2DReluBatchNorm(64, 3, 3, conv2)
    pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

    conv3 = Conv2DReluBatchNorm(128, 3, 3, pool2)
    conv3 = Conv2DReluBatchNorm(128, 3, 3, conv3)

    up8 = merge([UpSampling2D(size=(2, 2))(conv3), conv2], mode='concat', concat_axis=1)
    conv8 = Conv2DReluBatchNorm(64, 3, 3, up8)
    conv8 = Conv2DReluBatchNorm(64, 3, 3, conv8)

    up9 = merge([UpSampling2D(size=(2, 2))(conv8), conv1], mode='concat', concat_axis=1)
    conv9 = Conv2DReluBatchNorm(32, 3, 3, up9)
    conv9 = Conv2DReluBatchNorm(32, 3, 3, conv9)

    conv10 = Convolution2D(1, 1, 1, activation='sigmoid')(conv9)

    model = Model(input=inputs, output=conv10)

    model.compile(optimizer=Adam(), loss='binary_crossentropy', metrics=['accuracy'])

    return model


def train(dataset_name = None, num_epoch = None):
    
    #get dataset dir and train images names
    data_path = os.path.join('/notebooks/data/datasets', dataset_name)
    images_id = os.listdir(os.path.join(data_path, 'augment/augmented_train_images'))

    #read train images and masks
    train_images = []
    train_masks = []

    image_path = os.path.join(data_path, 'augment/augmented_train_images')
    mask_path = os.path.join(data_path, 'augment/augmented_train_masks')

    for img_id in images_id:
        train_images.append(imread(os.path.join(image_path, str(img_id)), as_grey=True).reshape((1,200,200)))
        train_masks.append(imread(os.path.join(mask_path, str(img_id).replace('jpg', 'png')), as_grey=True).reshape((1,200,200)))

    #pre-proc train masks to be in [0, 1]
    train_images = np.array(train_images)
    train_masks = np.array(train_masks)

    train_masks = train_masks.astype('float32')
    train_masks /= 255.

    #train model on train images on num_epoch epoch
    model = get_unet()
    model.fit(train_images, train_masks, batch_size=50, nb_epoch=num_epoch, verbose=1, shuffle=True)

    #save trained model
    model.save(os.path.join(data_path, str(dataset_name) + '.h5'))


def more_train(dataset_name = None, num_epoch = None):

    K.set_image_dim_ordering('th')

    #get dataset dir and train images names
    data_path = os.path.join('/notebooks/data/datasets', dataset_name)
    images_id = os.listdir(os.path.join(data_path, 'augment/augmented_train_images'))

    #read train images and masks
    train_images = []
    train_masks = []

    image_path = os.path.join(data_path, 'augment/augmented_train_images')
    mask_path = os.path.join(data_path, 'augment/augmented_train_masks')

    for img_id in images_id:
        train_images.append(imread(os.path.join(image_path, str(img_id)), as_grey=True).reshape((1,200,200)))
        train_masks.append(imread(os.path.join(mask_path, str(img_id).replace('jpg', 'png')), as_grey=True).reshape((1,200,200)))

    #pre-proc train masks to be in [0, 1]
    train_images = np.array(train_images)
    train_masks = np.array(train_masks)

    train_masks = train_masks.astype('float32')
    train_masks /= 255.

    #train model on more epoch
    model_path = os.path.join(data_path, dataset_name + ".h5")
    model = load_model(model_path)
    model.fit(train_images, train_masks, batch_size=50, nb_epoch=num_epoch, verbose=1, shuffle=True)


if __name__ == '__main__':
    if sys.argv[1] == 'train':
	   train(sys.argv[2], int(sys.argv[3]))
    if sys.argv[1] == 'more_train':
        more_train(sys.argv[2], int(sys.argv[3]))









